<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Vehicle_model;
use App\Vehicle_status;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vehicles.index')->with('vehicles', Vehicle::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle_models = Arr::sort(Vehicle_model::all(), function ($value) {
            return $value->make;
        });

        // dd($vehicle_models);
        return view('vehicles.create')->with('vehicle_models', $vehicle_models);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'plate_number' => 'unique:App\Vehicle,plate_number| required | size:6 | regex:/[a-zA-Z]{2}\d{4}/',
            'model' => 'required'
        ]);

        $vehicle = new Vehicle;
        $vehicle->plate_number = strtoupper($request->input('plate_number'));
        $vehicle->vehicle_model_id = $request->input('model');
        $vehicle->save();

        $request->session()->flash('success', 'Vehicle successfully added');
        
        return redirect(route('vehicles.show', ['vehicle' => $vehicle->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        return view('vehicles.show')->with('vehicle', $vehicle)->with('vehicle_statuses', Vehicle_status::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        return view('vehicles.edit')->with('vehicle', $vehicle)->with('vehicle_models', Vehicle_model::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        if($request->status !== null) {
            $vehicle->vehicle_status_id = $request->status;
            $vehicle->save();
            $request->session()->flash('success', 'Vehicle status successfully updated');

            return redirect(route('vehicles.show', ['vehicle' => $vehicle->id]));
        } else {
            $request -> validate([
                'plate_number' => 'required | size:6 | regex:/[a-zA-Z]{2}\d{4}/',
                'model' => 'required'
            ]);
    
            $vehicle->plate_number = strtoupper($request->input('plate_number'));
            $vehicle->vehicle_model_id = $request->input('model');
            $vehicle->save();
    
            $request->session()->flash('success', 'Vehicle successfully updated');
            return redirect(route('vehicles.edit', ['vehicle' => $vehicle->id]));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle, Request $request)
    {
        $vehicle->delete();
        $request->session()->flash('success', 'Vehicle deleted successfully');

        return redirect(route('vehicles.index'));
    }
}
