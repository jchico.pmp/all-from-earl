<?php

namespace App;
use Illuminate\Support\Arr;

use Illuminate\Database\Eloquent\Model;

class Vehicle_model extends Model
{
    public function vehicles() {
        return $this->hasMany(Vehicle::class);
    }

    public function vehicle_category() {
        return $this->belongsTo(Vehicle_category::class);
    }
}
