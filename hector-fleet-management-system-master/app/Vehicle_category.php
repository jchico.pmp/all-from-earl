<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle_category extends Model
{
    public function vehicles_models() {
        return $this->hasMany(Vehicle_model::class);
    }
}
