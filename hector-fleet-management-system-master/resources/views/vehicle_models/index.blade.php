@extends('layouts/app')
{{-- {{dd($vehicle_models)}} --}}

@section('title', 'Vehicles')

@section('content')

<div class="row mt-4 mb-0">

    
{{-- Start of Messages --}}
@include('messages')
{{-- End of Messages --}}

</div>

<div class="row">

   {{-- START MAIN --}}
    <div class="
    @if(Auth::user() !== null && Auth::user()->can('isMod'))
        col-sm-12
    @else
        col-lg-9
    @endif
    ">
        
        <div class="row">
            <div class="col-6">
                <h1>{{ Auth::user()->can('isUser')? "Choose Vehicles" : "Vehicle Models" }}</h1>
            </div>

            @can('isMod')
            <div class="col-6 text-right">
                <a href="{{route('vehicle_models.create')}}" class="btn btn-secondary ml-1 mb-2">&plus; Add a Vehicle Model</a>
            </div>
            @endcan


        </div>

        <div class="row pr-lg-2">
            
            {{-- Start of Vehicle Cards --}}
            @foreach($vehicle_models as $vehicle_model)
                @include('vehicle_models.inc.model_card')
            @endforeach
            {{-- End of Vehicle Cards --}}

        </div>
    </div>
    {{-- END MAIN --}}

        {{-- START CART --}}
        @can('isUser')
        <div class="col-sm-3 accordion d-none d-lg-block request-form" id="model-accordion">
            <div class="card">
                <div class="card-header p-0 bg-primary" id="model-list-toggle">
                    <button class="btn btn-primary w-100" data-toggle="collapse" data-target="#model-list">Vehicle Requests</button>
                </div>
                <div class="card-body collapse show" id="model-list" data-parent="#model-accordion">

                    {{-- Start Vehicle Items --}}
                    @if(Session::has('order_request'))
                    @foreach($request_models as $request_model)
                        <div class="row">
                            <div class="col-12">
                                {{-- DELETE Cart Item --}}
                                <form action="{{route('order_requests.destroy', ['order_request' => $request_model->id])}}" class="float-right" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn text-danger p-0" data-toggle="tooltip" title="Remove vehicle from request">
                                        <i class="far fa-times-circle"></i>
                                    </button>
                                </form>
                            </div>
                            <div class="col-12">
                                <div class="row mb-2">
                                    <div class="col-4">
                                        <img src="/public/{{$request_model->image}}" alt="" class="img-fluid">
                                    </div>
                                    <div class="col-8">
                                        <div class="row">
                                            <p class="mb-0">{{ "$request_model->make $request_model->name" }}</p>
                                        </div>
                                        <div class="row">
                                            <small>{{$request_model->year}}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <form action="{{route('order_requests.update', ['order_request' => $request_model->id])}}" method="post">
                                    @csrf
                                    @method('put')
                                    <label for="quantity" class="mb-1">Quantity:</label>
                                    <div class="form-group flex-fill row mb-0">
                                        <div class="col-12 input-group">
                                            <input type="number" class="form-control p-1 text-center" name="quantity" value="{{$request_model->quantity}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btn-sm w-100 h-100">Update</button>
                                            </div>
                                        </div>
                                        <div class="col-8 pl-0">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                    @endforeach

                    
                    @else
                    <span class="text-center d-block">Your request list is empty</span>
                    @endif
                    {{-- End Vehicle Items --}}
                </div>
                
                <div class="card-header bg-primary p-0" id="formAccordion">
                    <button class="btn btn-primary w-100" type="button" data-toggle="collapse" data-target="#formCollapse" aria-expanded="true" aria-controls="formCollapse">
                    <i class="fas fa-chevron-down"></i> Next
                    </button>
                </div>
                
                {{-- REQUEST FORM --}}
                <div class="card-body py-4 collapse" id="formCollapse" aria-labelledby="headingOne" data-parent="#model-accordion">
                    <form class="mb-3" action="{{ route('orders.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="borrow_date" class="mb-0">Borrow Date:</label>
                            <input type="date" name="borrow_date" class="form-control" value="{{ old('borrow_date')? old('borrow_date') : date('Y-m-d') }}">
                        </div>
                        <div class="form-group">
                            <label for="return_date" class="mb-0">Return Date:</label>
                            <input type="date" name="return_date" class="form-control" value="{{ old('return_date')? old('borrow_date') : date('Y-m-d') }}">
                        </div>
                        <div class="form-group">
                            <label for="purpose" class="mb-0">Purpose:</label>
                            <textarea name="purpose" id="purpose" rows="5" class="form-control">{{ old('purpose') }}</textarea>
                        </div>
                        <button class="btn btn-secondary w-100" type="submit">Submit Request</button>
                    </form>
                    <form action="#">
                        <button class="btn btn-danger w-100" type="submit">Cancel Request</button>
                    </form>
                </div>
                <div class="card-footer bg-primary p-3">
                </div>
            </div>
            
        </div>
        @endcan
        {{-- END CART --}}

</div>

<div class="row section px-3">
    
    
</div>
<div class="row mt-4">
    <div class="mx-auto">
        {{$vehicle_models->links()}}
    </div>
</div>

@endsection