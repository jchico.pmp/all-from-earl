@extends('layouts/app')

@section('title', 'Add a Vehicle Model')

@section('content')


<div class="row mt-4">
    
    {{-- Start of Messages --}}
    @include('messages')
    {{-- End of Messages --}}

    <div class="col-12 text-center">
        <h1>Add New Model</h1>
    </div>

    <div class="col-12 col-md-8 col-lg-6 mx-auto mt-5">
        
        <form action="{{route('vehicle_models.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            
            <div class="form-group">
                <label for="name">Model Name:</label>
                <input type="text" name="name" class="form-control" value="{{old('name')}}" required placeholder="Model Name">
            </div>

            <div class="form-group">
                <label for="make">Make:</label>
                <input type="text" name="make" class="form-control" value="{{old('make')}}" required placeholder="Model Make">
            </div>

            <div class="row">
                <div class="form-group col-6">
                    <label for="year">Year:</label>
                    <input type="number" name="year" class="form-control" value="{{old('year')}}" required placeholder="{{date('Y')}}">
                </div>
                
                <div class="form-group col-6">
                    <label for="year">Number of Seats:</label>
                    <input type="number" name="seats" class="form-control" value="{{old('seats')}}" required placeholder="Number of Seats">
                </div>
            </div>

            <div class="form-group mb-3">
                <label for="category">Category:</label>
                <select name="category" class="form-control mb-2" required>
                    <option value="" selected>Choose a category</option>
                    {{-- Start of Model List --}}
                    @foreach($categories as $category)
                    <option value="{{$category->id}}"
                        {{old('category') == $category->id ? "selected" : ""}}
                        >
                        {{"$category->name"}}
                    </option>
                    @endforeach
                    {{-- End of Model List --}}

                </select>
            </div>

            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" id="image" class="form-control-file">
            </div>

            <div class="form-group mb-5">
                <label for="description">Description:</label>
                <textarea name="description" rows="10" class="form-control" placeholder="Description">{{old('description')}}</textarea>
            </div>

            <button class="btn btn-primary w-100 mb-3" type="submit">Submit New Vehicle Model</button>

        </form>

        <a href="{{route('vehicle_models.index')}}" class="btn btn-secondary w-100">Cancel</a>

    </div>
</div>


@endsection