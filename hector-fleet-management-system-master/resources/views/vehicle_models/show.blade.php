@extends('layouts/app')
{{-- {{dd($vehicle_models)}} --}}

@section('title', 'Vehicles')

@section('content')

<div class="row my-4">
    
    {{-- Start of Messages --}}
    @include('messages')
    {{-- End of Messages --}}

    
</div>

<div class="card">   
    <div class="card-header order">
        <div class="row px-3">
            <a href="{{route('vehicle_models.index')}}" class="btn btn-primary mr-3">Back</a>
            <h2 class="m-0 d-inline">Vehicle Model</h2>
        </div>
    </div> 
    <div class="card-body bordered p-5">
        <div class="row">
            <div class="col-lg-6">
                <img src="/public/{{$vehicle_model->image}}" alt="{{"$vehicle_model->make $vehicle_model->name $vehicle_model->year"}}" class="img-fluid">
            </div>
        
            <div class="col-lg-6">
                <h3>{{"$vehicle_model->make $vehicle_model->name $vehicle_model->year"}}</h3>
                <p class="mb-5">{{$vehicle_model->vehicle_category->name}} | Seats: {{$vehicle_model->seats}}</p>
                <p class="mb-5">{{$vehicle_model->description}}</p>
                
                @can('isMod')
                <div class="col-12 text-right">
                    <a href="{{route('vehicle_models.edit', ['vehicle_model' => $vehicle_model->id])}}" class="btn btn-primary text-light">Update Vehicle Model</a>
                    <form action="{{route('vehicle_models.destroy', ['vehicle_model' => $vehicle_model->id])}}" method="post" class="d-inline">
                        @csrf
                        @method('delete')
                        <button class="btn btn-danger w-lg-25" type="submit">Delete Vehicle Model</button>
                    </form>
                </div>
                @endcan
            </div>
        </div>
    </div>
    <div class="card-body order">

    </div>
</div>

{{-- Start Vehicle List --}}
@can('isMod')
<div class="row mt-3 border p-5 d-none d-lg-block">
    <div class="col-12">
        <h3 class="mb-4">Vehicle List</h3>
    </div>

    <div class="col-12">
        <table class="table table-sm table-hover text-center">
            <thead>
                <tr>
                    <th>Plate Number</th>
                    <th>Status</th>
                    <th>Vehicle Details</th>
                </tr>
            </thead>

            <tbody>
                {{-- Start of Vehicle Info --}}
                @foreach($vehicles as $vehicle)
                <tr>
                    <td  class="align-middle">{{$vehicle->plate_number}}</td>
                    <td class="text-light font-weight-bold">
                        <div class=" p-2
                            @switch($vehicle->vehicle_status_id)
                                @case(1)
                                    bg-success
                                    @break
                                @case(2)
                                    bg-danger
                                    @break
                                @case(3)
                                    bg-secondary
                                    @break
                            @endswitch
                        ">
                        {{strtoupper($vehicle->vehicle_status->name)}}
                        </div>
                    </td>
                    <td class="align-middle">
                        <a href="{{route('vehicles.show', ['vehicle' => $vehicle->id])}}" class="btn btn-sm btn-outline-primary">View Vehicle Details</a>
                    </td>
                </tr>
                @endforeach
                {{-- End of Vehicle Info --}}
            </tbody>
        </table>
    </div>
</div>
@endcan
{{-- End Vehicle List --}}

@endsection