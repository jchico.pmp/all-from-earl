@extends('layouts.app')

@section('content')

{{-- Start of Messages --}}
@include('messages')
{{-- End of Messages --}}

<!-- MAIN CONTENT -->
<div class="row">
    <div class="col-12 col-lg-8 mx-auto">
        <div class="accordion w-100" id="order-list">
            {{-- Start Order Item List --}}
            @foreach($orders as $order)
                @can('viewOrder', $order)
                <div class="card">
                    <div class="order card-header px-0" id="order-{{ $order->id }}">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#details-{{ $order->id }}" aria-expanded="true" aria-controls="details-{{ $order->id }}">
                                <h5 class="mb-0">{{$order->order_number}}</h5>
                            </button>
                            <div class="d-inline text-light float-right mb-3 mr-3 px-2 py-1
                            @if($order->order_status->id == 2 && date('Y-m-d') > $order->return_date)
                                bg-danger
                            @elseif($order->order_status_id === 1)
                                bg-grey
                            @elseif($order->order_status_id === 2)
                                bg-success
                            @elseif($order->order_status_id === 3)
                                bg-primary
                            @elseif($order->order_status_id === 4)
                                bg-warning
                            @endif
                            
                        ">
                            {{ $order->order_status->id == 2 && date('Y-m-d') > $order->return_date? "OVERDUE" : $order->order_status->name }}
                        </div>
                        <div class="col-12 text-dark">
                            {{date('M-d-Y', strtotime($order->created_at))}} | {{ ucwords($order->user->firstname . " " . $order->user->lastname)}}
                        </div>
                    </div>
                
                    <div id="details-{{ $order->id }}" class="collapse" aria-labelledby="order-ID" data-parent="#order-list">
                        <div class="card-body row py-0 bordered">
                            <table class="table col-sm-6 my-0">
                                <tr class="row px-3">
                                    <td class="col-6">Vehicles Requested:</td>
                                    <td class="col-6">{{ count($order->vehicle_models()->get()) }}</td>
                                </tr>
                                <tr class="row px-3">
                                    <td class="col-6">Last Update:</td>
                                    {{-- {{ dd($order->updated_at) }} --}}
                                    <td class="col-6">{{date('F d, Y', strtotime($order->updated_at))}}</td>
                                </tr>
                            </table>
                            <table class="table col-sm-6 my-0">
                                <tr class="row px-3">
                                    <td class="col-6">Borrow Date:</td>
                                    <td class="col-6">{{date('F d, Y', strtotime($order->borrow_date))}}</td>
                                </tr>
                                <tr class="row px-3">
                                    
                                    <td class="col-6">Return Date:</td>
                                    <td class="col-6">{{date('F d, Y', strtotime($order->return_date))}}</td>
                                </tr>
                            </table>
                        </div>
                        
                        <div class="order card-footer">
                            <form action="{{ route('orders.destroy', ['order' => $order->id]) }}" method="post" class="text-right">
                                @csrf
                                @method('delete')
                                <a href="{{ route('orders.show', ['order' => $order->id]) }}" class="btn btn-primary btn-sm">View Details</a>
                                <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
                @endcan
            @endforeach    
            {{-- End Order Item List --}}
        </div>
    </div>
</div>

@endsection