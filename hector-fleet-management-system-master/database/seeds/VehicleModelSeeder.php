<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_models')->insert([
            'name' => 'Vios',
            'make' => 'Toyota',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_1.png',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis incidunt dolorum reprehenderit quaerat qui labore illo quibusdam? Labore!',
            'seats' => '5',
            'vehicle_category_id' => '2'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'City',
            'make' => 'Honda',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_2.png',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas, eos ipsum officiis repellat nisi laboriosam deserunt accusamus maxime, temporibus aliquam eius.',
            'seats' => '5',
            'vehicle_category_id' => '2'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Lancer',
            'make' => 'Mitsubishi',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_3.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quis quod, nostrum exercitationem sint, dolorem perspiciatis architecto accusamus impedit mollitia in nemo iste excepturi voluptates fugiat eaque explicabo consequuntur!',
            'seats' => '5',
            'vehicle_category_id' => '2'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Aerox',
            'make' => 'Yamaha',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_4.png',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor numquam quisquam pariatur repudiandae?' ,
            'seats' => '2',
            'vehicle_category_id' => '1'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Click',
            'make' => 'Honda',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_5.png',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium libero modi dolore magnam cumque quae!' ,
            'seats' => '2',
            'vehicle_category_id' => '1'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Hiace',
            'make' => 'Toyota',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_6.png',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae alias iste in voluptatum commodi ea porro labore, minus voluptatem quidem, magni quam eligendi?',
            'seats' => '16',
            'vehicle_category_id' => '5'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Montero',
            'make' => 'Mitsubishi',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_7.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum excepturi velit nostrum rerum necessitatibus, fugit blanditiis quidem iusto illo.',
            'seats' => '7',
            'vehicle_category_id' => '3'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Alphard',
            'make' => 'Toyota',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_8.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum excepturi velit nostrum rerum necessitatibus, fugit blanditiis quidem iusto illo.',
            'seats' => '7',
            'vehicle_category_id' => '4'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Carry',
            'make' => 'Suzuki',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_9.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum excepturi velit nostrum rerum necessitatibus, fugit blanditiis quidem iusto illo.',
            'seats' => '2',
            'vehicle_category_id' => '6'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Forward',
            'make' => 'Isuzu',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_10.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum excepturi velit nostrum rerum necessitatibus, fugit blanditiis quidem iusto illo.',
            'seats' => '3',
            'vehicle_category_id' => '7'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Auman',
            'make' => 'Foton',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_11.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum excepturi velit nostrum rerum necessitatibus, fugit blanditiis quidem iusto illo.',
            'seats' => '3',
            'vehicle_category_id' => '8'
        ]);
    }
}
