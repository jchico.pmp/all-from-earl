<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_categories')->insert([
            'name' => 'Motorcycle',
        ]);
        DB::table('vehicle_categories')->insert([
            'name' => 'Sedan',
        ]);
        DB::table('vehicle_categories')->insert([
            'name' => 'SUV',
        ]);
        DB::table('vehicle_categories')->insert([
            'name' => 'Minivan',
        ]);
        DB::table('vehicle_categories')->insert([
            'name' => 'Van',
        ]);
        DB::table('vehicle_categories')->insert([
            'name' => 'Mini Truck',
        ]);
        DB::table('vehicle_categories')->insert([
            'name' => 'Truck',
        ]);
        DB::table('vehicle_categories')->insert([
            'name' => 'Big Truck',
        ]);
    }
}
