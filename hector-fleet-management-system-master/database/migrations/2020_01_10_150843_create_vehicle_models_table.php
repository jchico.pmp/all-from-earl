<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('make');
            $table->year('year');
            $table->string('image')->default('model_images/default.png');
            $table->text('description');
            $table->integer('seats');

            $table->unsignedBigInteger('vehicle_category_id')->nullable();
            $table->foreign('vehicle_category_id')
                ->references('id')
                ->on('vehicle_categories')
                ->onDelete('set null')
                ->onUpdate('set Null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_models');
    }
}
